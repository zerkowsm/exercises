Immutability exercises
=========================
**Description**

Simple immutable list, set and map classes.

**Compiling**

    $mvn clean install
    
OR

    $gradlew clean install

**Configuration**

**Running**