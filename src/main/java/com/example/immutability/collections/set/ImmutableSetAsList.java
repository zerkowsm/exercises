package com.example.immutability.collections.set;

import com.example.immutability.collections.list.BaseImmutableList;

import java.util.Collection;

/**
 * Immutable set delegate as immutable list object.
 * <p>
 * Created by mjzi on 2016-07-01
 */
final class ImmutableSetAsList<E> extends BaseImmutableList<E> {

    private final BaseImmutableSet<E> immutableSet;

    ImmutableSetAsList(BaseImmutableSet<E> immutableSet) {
        this.immutableSet = immutableSet;
    }

    @Override
    public boolean isEmpty() {
        return immutableSet.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return immutableSet.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return immutableSet.containsAll(c);
    }

    @Override
    public int size() {
        return immutableSet.size();
    }

    @Override
    public E get(int index) {
        return immutableSet.get(index);
    }

}
