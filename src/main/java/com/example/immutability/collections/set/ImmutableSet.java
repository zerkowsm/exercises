package com.example.immutability.collections.set;

import com.example.immutability.collections.iterator.ImmutableIterator;

import java.util.Set;

/**
 * Simple immutable {@link Set} class. For more fine-grained implementation please check Guava library and com.google.common.collect.ImmutableSet class.
 * <p>
 * Created by mjzi on 2016-06-15
 */
public final class ImmutableSet<E> extends BaseImmutableSet<E> {

    private final Object[] array;
    private final int size;

    ImmutableSet(Object[] array) {
        this.array = array;
        this.size = array.length;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ImmutableIterator<E> iterator() {
        return new ImmutableIterator<E>(size, 0) {
            @Override
            protected E get(int index) {
                return (E) array[index];
            }
        };
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected E get(int index) {
        return (E) array[index];
    }

}
