package com.example.immutability.collections.set;

import com.example.immutability.collections.iterator.ImmutableIterator;

/**
 * Just for the exercises purposes. Class not used currently (worse design than the current one)
 *
 * @deprecated Use {@link ImmutableIterator} instead
 * <p>
 * Created by mjzi on 2016-06-17
 */
@Deprecated
public final class ImmutableSetIterator<E> extends ImmutableIterator<E> {

    private final ImmutableSet<E> immutableSet;

    ImmutableSetIterator(ImmutableSet<E> immutableSet, int index) {
        super(immutableSet.size(), index);
        this.immutableSet = immutableSet;
    }

    /**
     * Just for the exercises purposes
     *
     * @param index a set's underlying list element index to be returned
     * @return a set's underlying list element for a given {@code index}
     */
    @Override
    protected E get(int index) {
        return immutableSet.asList().get(index);
    }

}
