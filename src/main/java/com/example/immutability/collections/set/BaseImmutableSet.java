package com.example.immutability.collections.set;

import com.example.immutability.collections.ImmutableCollection;
import com.example.immutability.collections.iterator.BaseImmutableIterator;
import com.example.immutability.collections.list.BaseImmutableList;
import org.apache.commons.collections4.SetUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Base abstract immutable set class.
 * <p>
 * Created by mjzi on 2016-06-24
 */
public abstract class BaseImmutableSet<E> extends ImmutableCollection<E> implements Set<E> {

    private BaseImmutableList<E> asList;

    public static <E> BaseImmutableSet<E> from(Set<? extends E> set) {
        return new ImmutableSet<>(set.toArray());
    }

    public static <E> BaseImmutableSet<E> from(Collection<? extends E> collection) {
        return new ImmutableSet<>(collection.stream().collect(Collectors.toSet()).toArray());
    }

    public static <E> BaseImmutableSet<E> from(Object[] array) {
        return new ImmutableSet<>(Arrays.stream(array).collect(Collectors.toSet()).toArray());
    }

    public BaseImmutableList<E> asList() {
        BaseImmutableList<E> immutableList = asList;
        return (immutableList == null) ? asList = new ImmutableSetAsList<>(this) : immutableList;
    }

    @Override
    public abstract BaseImmutableIterator<E> iterator();

    protected abstract E get(int index);

    @Override
    public int hashCode() {
        int hashCode = 0;
        int size = size();
        for (int i = 0; i < size; i++) {
            hashCode = hashCode + (get(i) == null ? 0 : get(i).hashCode());
        }
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Set && SetUtils.isEqualSet(this, (Set) obj);
    }

}
