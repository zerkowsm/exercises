package com.example.immutability.collections.iterator;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by mjzi on 2016-06-17
 */
public abstract class ImmutableIterator<E> extends BaseImmutableIterator<E> implements ListIterator<E> {

    private final int size;
    private int cursor;

    public ImmutableIterator(int size, int index) {
        this.size = size;
        this.cursor = index;
    }

    protected abstract E get(int index);

    @Override
    public final boolean hasPrevious() {
        return cursor > 0;
    }

    @Override
    public final E previous() {
        if (!hasPrevious()) {
            throw new NoSuchElementException();
        }
        return get(--cursor);
    }

    @Override
    public final int nextIndex() {
        return cursor;
    }

    @Override
    public final int previousIndex() {
        return cursor - 1;
    }

    @Override
    public final boolean hasNext() {
        return cursor < size;
    }

    @Override
    public final E next() {
        if (hasNext()) {
            return get(cursor++);
        }
        throw new NoSuchElementException();
    }

    @Override
    public final void set(E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final void add(E e) {
        throw new UnsupportedOperationException();
    }

}
