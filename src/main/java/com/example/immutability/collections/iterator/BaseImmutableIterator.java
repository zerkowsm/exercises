package com.example.immutability.collections.iterator;

import java.util.Iterator;

/**
 * Created by mjzi on 2016-06-16
 */
public abstract class BaseImmutableIterator<E> implements Iterator<E> {

	@Override
	public final void remove() {
		throw new UnsupportedOperationException();
	}

}
