package com.example.immutability.collections;

import com.example.immutability.collections.list.BaseImmutableList;
import com.example.immutability.collections.list.ImmutableList;

import java.util.Collection;

/**
 * Just for the exercises purposes. Immutable collection delegate as immutable list object. Not used at the moment.
 * <p>
 * Created by mjzi on 2016-06-30
 */
public final class ImmutableAsList<E> extends BaseImmutableList<E> {

    private final ImmutableCollection<? extends E> immutableCollection;
    //private final Object[] array;
    private final BaseImmutableList<E> immutableList;

    public ImmutableAsList(ImmutableCollection<? extends E> immutableCollection, Object[] array) {
        this.immutableCollection = immutableCollection;
        //this.array = array;

//        I hate to comment the code but this is only exercises...
//        Below assignment is to make sure that we do not deal with potentially live array. The drawback is that it requires additional copy.
//        Considering changing access level to package level (it will require collections flat package to be used from concrete collections types)
//        and the fact that we work on a closed library you could potentially decide to rely on given array and assuming it is already
//        immutable at that stage and comment out the below assignment.
//        But like master Yoda says: with great power comes great responsibility.
        this.immutableList = ImmutableList.from(array);
    }

    @Override
    public boolean isEmpty() {
        return immutableCollection.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return immutableCollection.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return immutableCollection.containsAll(c);
    }

    @Override
    public int size() {
        return immutableCollection.size();
    }

    @Override
    public E get(int index) {
//        return (E) array[index];
//        Check the comment in the constructor
        return immutableList.get(index);
    }

}
