package com.example.immutability.collections.map;

import com.example.immutability.collections.ImmutableCollection;
import com.example.immutability.collections.set.BaseImmutableSet;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Simple immutable {@link Map} class. For more fine-grained implementation please check Guava library and com.google.common.collect.ImmutableMap class.
 * <p>
 * Created by mjzi on 2016-06-23
 */
public final class ImmutableMap<K, V> implements Map<K, V> {

    public static final int DEFAULT_INITIAL_CAPACITY = 16; //initial and big enough power of 2
    public static final int MAXIMUM_CAPACITY = 1 << 30; //power of 2 - Integer.MAX_VALUE/2 + 1

    private final Entry<K, V>[] entries;
    private final ImmutableNode<K, V>[] table;
    private final int mask;

    private ImmutableCollection<V> values;
    private BaseImmutableSet<Entry<K, V>> entrySet;
    private BaseImmutableSet<K> keySet;

    @SuppressWarnings("unchecked")
    private ImmutableMap(Collection<? extends Entry<? extends K, ? extends V>> entries) {
        this.entries = (Entry<K, V>[]) toArray(entries);

        int size = this.entries.length;
        if (size < DEFAULT_INITIAL_CAPACITY) {
            size = DEFAULT_INITIAL_CAPACITY;
        } else if (size > MAXIMUM_CAPACITY) {
            size = MAXIMUM_CAPACITY;
        } else {
            size = tableSizeFor(size);
        }

        this.table = new ImmutableNode[size];
        this.mask = size - 1;

        populateMapEntries();
    }

    private Entry<?, ?>[] toArray(Collection<? extends Entry<? extends K, ? extends V>> collection) {
        return collection.toArray(new Entry<?, ?>[0]);
    }

    public static <K, V> ImmutableMap<K, V> from(Map<? extends K, ? extends V> map) {
        return new ImmutableMap<>(map.entrySet());
    }

    /**
     * Taken from {@link java.util.HashMap} implementation. Returns the next power of two size for the given target capacity.
     * Eg. It returns 32 for a given capacity as 20.
     */
    private static int tableSizeFor(int capacity) {
        int n = capacity - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }

    @Override
    public int size() {
        return entries.length;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return get(key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        return values().contains(value);
    }

    @Override
    public V get(Object key) {
        ImmutableNode<K, V> immutableNode;
        return (immutableNode = getNode(hash(key), key)) == null ? null : immutableNode.value;
    }

    @Override
    public final V put(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final V remove(Object key) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link #remove} method
     */
    @Override
    public final boolean remove(Object key, Object value) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link #put(Object, Object)} method
     */
    @Override
    public final boolean replace(K key, V oldValue, V newValue) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link #put(Object, Object)} method
     */
    @Override
    public V replace(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final void putAll(Map<? extends K, ? extends V> m) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link Map.Entry} set method
     */
    @Override
    public final void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link #get} method
     */
    @Override
    public final V putIfAbsent(K key, V value) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link #put(Object, Object)} method
     */
    @Override
    public final V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link #remove(Object)} and {@link #put(Object, Object)} methods
     */
    @Override
    public final V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link #put} and {@link #remove(Object)} methods
     */
    @Override
    public final V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Map} default method override (since 1.8) so we do not rely implicitly on {@link #remove(Object)} and {@link #put(Object, Object)} methods
     */
    @Override
    public final V merge(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public BaseImmutableSet<K> keySet() {
        BaseImmutableSet<K> immutableSet = keySet;
        return (immutableSet == null) ? keySet = new ImmutableMapKeySet<>(this) : immutableSet;
    }

    @Override
    public Collection<V> values() {
        ImmutableCollection<V> immutableCollection = values;
        return (immutableCollection == null) ? values = new ImmutableMapValues<>(this) : immutableCollection;
    }

    @Override
    public BaseImmutableSet<Entry<K, V>> entrySet() {
        BaseImmutableSet<Entry<K, V>> immutableSet = entrySet;
        return (immutableSet == null) ? entrySet = new ImmutableMapEntrySet<>(this, entries) : immutableSet;
    }

    /**
     * Original hash function of {@link java.util.HashMap} hash method
     */
    private static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    private void populateMapEntries() {
        int entriesNumber = entries.length;
        for (int i = 0; i < entriesNumber; i++) {
            Entry<K, V> entry = entries[i];
            int index = mask & hash(entry.getKey());
            ImmutableNode<K, V> existingEntry = table[index];
            ImmutableNode<K, V> newEntry;
            if (existingEntry != null) {
                newEntry = new ImmutableNode<>(entry.getKey(), entry.getValue(), entry.getKey().hashCode(), existingEntry);
            } else {
                newEntry = new ImmutableNode<>(entry.getKey(), entry.getValue(), entry.getKey() != null ? entry.getKey().hashCode() : 0, null);
            }
            table[index] = newEntry;
            entries[i] = newEntry;
        }
    }

    private ImmutableNode<K, V> getNode(int hash, Object key) {
        ImmutableNode<K, V> entry = table[mask & hash];
        if (key == null) {
            return entry;
        }
        int keyHashCode = key.hashCode();
        if (entry != null) {
            ImmutableNode<K, V> next = (ImmutableNode<K, V>) entry.getNext();
            while (next != null) {
                K k = next.key;
                if (k == null || next.hash == keyHashCode && (k == key || k.equals(key))) {
                    return next;
                }
                next = (ImmutableNode<K, V>) next.getNext();
            }
            K k = entry.key;
            if (k == null || entry.hash == keyHashCode && (k == key || k.equals(key))) {
                return entry;
            }
        }
        return null;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Object o : this.entrySet()) {
            hashCode = hashCode + (o == null ? 0 : o.hashCode());
        }
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj instanceof Map) {
            return this.entrySet().equals(((Map<?, ?>) obj).entrySet());
        }
        return false;
    }

    /**
     * Implementation of {@link Map.Entry} for {@link ImmutableMap}
     */
    static class ImmutableNode<K, V> implements Entry<K, V> {

        private final K key;
        private final V value;
        private final int hash;
        private final Entry<K, V> next;

        /**
         * @param key   entry key
         * @param value entry value
         * @param hash  entry key's precomputed hash so it can be cached and don't have to be recalculated each call
         * @param next  next entry with the same key's hashcode as current one
         */
        ImmutableNode(K key, V value, int hash, Entry<K, V> next) {
            this.key = key;
            this.value = value;
            this.hash = hash;
            this.next = next;
        }

        @Override
        public final K getKey() {
            return key;
        }

        @Override
        public final V getValue() {
            return value;
        }

        @Override
        public final V setValue(V value) {
            throw new UnsupportedOperationException();
        }

        final Entry<K, V> getNext() {
            return next;
        }

        @Override
        public final boolean equals(Object object) {
            if (object instanceof Entry) {
                Entry<?, ?> that = (Entry<?, ?>) object;
                return Objects.equals(this.getKey(), that.getKey())
                        && Objects.equals(this.getValue(), that.getValue());
            }
            return false;
        }

        @Override
        public final int hashCode() {
            K key = getKey();
            V value = getValue();
            return ((key == null) ? 0 : key.hashCode()) ^ ((value == null) ? 0 : value.hashCode());
        }

        @Override
        public String toString() {
            return getKey() + "=" + getValue();
        }

    }

}
