package com.example.immutability.collections.map;

import com.example.immutability.collections.iterator.BaseImmutableIterator;
import com.example.immutability.collections.set.BaseImmutableSet;

import java.util.Map;

/**
 * {@code keySet()} implementation for {@link ImmutableMap}.
 * <p>
 * Created by mjzi on 2016-06-24
 */
final class ImmutableMapKeySet<K, V> extends BaseImmutableSet<K> {

    private final ImmutableMap<K, V> immutableMap;

    ImmutableMapKeySet(ImmutableMap<K, V> immutableMap) {
        this.immutableMap = immutableMap;
    }

    @Override
    public BaseImmutableIterator<K> iterator() {
        return new BaseImmutableIterator<K>() {

            BaseImmutableIterator<Map.Entry<K, V>> iterator = immutableMap.entrySet().iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public K next() {
                return iterator.next().getKey();
            }
        };

    }

    @Override
    public int size() {
        return immutableMap.size();
    }

    @Override
    protected K get(int index) {
        return immutableMap.entrySet().asList().get(index).getKey();
    }

}
