package com.example.immutability.collections.map;

import com.example.immutability.collections.iterator.BaseImmutableIterator;
import com.example.immutability.collections.set.BaseImmutableSet;

import java.util.Map;

/**
 * {@code entrySet()} implementation for {@link ImmutableMap}.
 * <p>
 * Created by mjzi on 2016-06-24
 */
final class ImmutableMapEntrySet<K, V> extends BaseImmutableSet<Map.Entry<K, V>> {

    private final ImmutableMap<K, V> immutableMap;
    private final Map.Entry<K, V>[] entries;

    ImmutableMapEntrySet(ImmutableMap<K, V> immutableMap, Map.Entry<K, V>[] entries) {
        this.immutableMap = immutableMap;
        this.entries = entries;
    }

    @Override
    public BaseImmutableIterator<Map.Entry<K, V>> iterator() {
        return asList().iterator();
    }

    @Override
    public int size() {
        return immutableMap.size();
    }

    @Override
    protected Map.Entry<K, V> get(int index) {
        return entries[index];
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof Map.Entry) {
            Map.Entry<?, ?> entry = (Map.Entry<?, ?>) o;
            V value = immutableMap.get(entry.getKey());
            return value != null && value.equals(entry.getValue());
        }
        return false;
    }

}
