package com.example.immutability.collections.map;

import com.example.immutability.collections.ImmutableCollection;
import com.example.immutability.collections.iterator.BaseImmutableIterator;

import java.util.Map;

/**
 * {@code values()} implementation for {@link ImmutableMap}.
 * <p>
 * Created by mjzi on 2016-06-24
 */
final class ImmutableMapValues<K, V> extends ImmutableCollection<V> {

    private final ImmutableMap<K, V> immutableMap;

    ImmutableMapValues(ImmutableMap<K, V> immutableMap) {
        this.immutableMap = immutableMap;
    }

    @Override
    public BaseImmutableIterator<V> iterator() {
        return new BaseImmutableIterator<V>() {

            BaseImmutableIterator<Map.Entry<K, V>> iterator = immutableMap.entrySet().iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public V next() {
                return iterator.next().getValue();
            }
        };

    }

    @Override
    public int size() {
        return immutableMap.size();
    }

}
