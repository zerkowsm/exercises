package com.example.immutability.collections.list;

import com.example.immutability.collections.iterator.ImmutableIterator;
import com.example.immutability.collections.ImmutableCollection;
import com.example.immutability.collections.iterator.BaseImmutableIterator;
import org.apache.commons.collections4.ListUtils;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * Base abstract immutable list class.
 * <p>
 * Created by mjzi on 2016-06-30
 */
public abstract class BaseImmutableList<E> extends ImmutableCollection<E> implements List<E> {

    public static <E> BaseImmutableList<E> from(Collection<? extends E> collection) {
        return new ImmutableList<>(collection.toArray());
    }

    public static <E> BaseImmutableList<E> from(Object[] array) {
        return new ImmutableList<>(Arrays.copyOf(array, array.length));
    }

    @Override
    public BaseImmutableIterator<E> iterator() {
        return listIterator();
    }

    @Override
    public final boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final E set(int index, E element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final void add(int index, E element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final E remove(int index) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link Collection} default method override (since 1.8) so we do not rely implicitly on {@link BaseImmutableIterator} remove method
     *
     * @param filter a predicate which returns {@code true} for elements to be removed
     * @return UnsupportedOperationException
     */
    public final boolean removeIf(Predicate<? super E> filter) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link List} default method override (since 1.8) so we do not rely implicitly on {@link #set} method
     *
     * @param operator the operator to apply to each element
     */
    public final void replaceAll(UnaryOperator<E> operator) {
        throw new UnsupportedOperationException();
    }

    /**
     * Explicit {@link List} default method override (since 1.8) so we do not rely implicitly on {@link #set} method
     *
     * @param c the {@code Comparator} used to compare list elements.
     */
    public final void sort(Comparator<? super E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        ListIterator<E> iterator = listIterator();
        while (iterator.hasNext()) {
            if (Objects.equals(o, iterator.next())) {
                return iterator.previousIndex();
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        ListIterator<E> iterator = listIterator(size());
        while (iterator.hasPrevious()) {
            if (Objects.equals(o, iterator.previous())) {
                return iterator.nextIndex();
            }
        }
        return -1;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public ImmutableIterator<E> listIterator() {
        return listIterator(0);
    }

    @Override
    public ImmutableIterator<E> listIterator(int index) {
        return new ImmutableIterator<E>(size(), index) {
            @Override
            protected E get(int index) {
                return BaseImmutableList.this.get(index);
            }
        };
    }

    /**
     * @param fromIndex low inclusive index of the requested subList
     * @param toIndex   high exclusive index of the requested subList
     * @return a list view of the specified range
     */
    @Override
    public BaseImmutableList<E> subList(int fromIndex, int toIndex) {
        if (isSubListParamsRangeInvalid(fromIndex, toIndex)) {
            throw new IllegalArgumentException();
        }
        return new BaseImmutableList<E>.SubList(fromIndex, toIndex - fromIndex);
    }

    @Override
    public int hashCode() {
        int hashCode = 1;
        int size = size();
        for (int i = 0; i < size; i++) {
            hashCode = 31 * hashCode + (get(i) == null ? 0 : get(i).hashCode());
        }
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof List && ListUtils.isEqualList(this, (List) obj);
    }

    private boolean isSubListParamsRangeInvalid(int fromIndex, int toIndex) {
        return fromIndex < 0 || toIndex > size() || toIndex < fromIndex;
    }

    private class SubList extends BaseImmutableList<E> {
        final int offset;
        final int size;

        SubList(int offset, int size) {
            this.offset = offset;
            this.size = size;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public E get(int index) {
            return BaseImmutableList.this.get(index + offset);
        }

        @Override
        public BaseImmutableList<E> subList(int fromIndex, int toIndex) {
            return BaseImmutableList.this.subList(fromIndex + offset, toIndex + offset);
        }

    }

}
