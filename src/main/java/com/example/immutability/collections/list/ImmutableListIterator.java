package com.example.immutability.collections.list;

import com.example.immutability.collections.iterator.ImmutableIterator;

/**
 * Just for the exercises purposes. Class not used currently (worse design than the current one)
 *
 * @deprecated Use {@link ImmutableIterator} instead
 * <p>
 * Created by mjzi on 2016-06-16
 */
@Deprecated
public final class ImmutableListIterator<E> extends ImmutableIterator<E> {

    private final ImmutableList<E> immutableList;

    ImmutableListIterator(ImmutableList<E> immutableList, int index) {
        super(immutableList.size(), index);
        this.immutableList = immutableList;
    }

    /**
     * Just for the exercises purposes
     *
     * @param index element index to be returned
     * @return a list element for a given {@code index}
     */
    @Override
    protected E get(int index) {
        return immutableList.get(index);
    }

}
