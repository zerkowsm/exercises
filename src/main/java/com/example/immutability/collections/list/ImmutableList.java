package com.example.immutability.collections.list;

import java.util.List;

/**
 * Simple immutable {@link List} class. For more fine-grained implementation please check Guava library and com.google.common.collect.ImmutableList class.
 * <p>
 * Created by mjzi on 2016-06-15
 */
public final class ImmutableList<E> extends BaseImmutableList<E> implements List<E> {

    private final Object[] array;
    private final int size;

    private ImmutableList(Object[] array, int size) {
        this.array = array;
        this.size = size;
    }

    ImmutableList(Object[] array) {
        this(array, array.length);
    }

    @Override
    @SuppressWarnings("unchecked")
    public E get(int index) {
        return (E) array[index];
    }

    @Override
    public int size() {
        return size;
    }

}
