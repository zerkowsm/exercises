package com.example.immutability.collections;

import com.example.immutability.collections.iterator.BaseImmutableIterator;

import java.util.AbstractCollection;
import java.util.Collection;

/**
 * Base abstract immutable collection class.
 * <p>
 * Created by mjzi on 2016-06-15
 */
public abstract class ImmutableCollection<E> extends AbstractCollection<E> {

    @Override
    public abstract BaseImmutableIterator<E> iterator();

    @Override
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }

}
