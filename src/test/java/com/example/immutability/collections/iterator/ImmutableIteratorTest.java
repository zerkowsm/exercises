package com.example.immutability.collections.iterator;

import com.example.immutability.collections.iterator.ImmutableIterator;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.NoSuchElementException;

/**
 * Created by mjzi on 2016-07-01
 */
public class ImmutableIteratorTest {

    private static String[] array;
    private static int size;

    @BeforeClass
    public static void setUp() {
        array = new String[]{"Red", "Yellow", "Red"};
        size = array.length;
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowChangingElementValue() {
        ImmutableIterator<String> it = getIteratorToTest();
        while (it.hasNext()) {
            if (it.hasPrevious()) {
                it.set("Impossible");
            }
            it.next();
        }
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldReturnNextElementsProperly() {
        ImmutableIterator<String> it = getIteratorToTest();
        while (it.hasNext()) {
            it.next();
        }
        it.next();
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldReturnPreviousElementsProperly() {
        getIteratorToTest().previous();
    }

    private ImmutableIterator<String> getIteratorToTest() {
        return new ImmutableIterator<String>(size, 0) {
            @Override
            protected String get(int index) {
                return array[index];
            }
        };
    }

}