package com.example.immutability.collections.map;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.immutability.collections.TestUtils.forEach;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Created by mjzi on 2016-06-23
 */
public class ImmutableMapTest {

    private static ImmutableMap<Integer, String> immutableMap;

    @BeforeClass
    public static void setUp() throws Exception {
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "Green");
        }};
        immutableMap = ImmutableMap.from(map);
    }

    @AfterClass
    public static void tearDown() {
        assertThat("Actual collection size initiated before the test suite is different from the one got after the test suite execution", immutableMap.size(), equalTo(3));
    }

    @Test
    public void shouldCreateImmutableMap_fromMapWithNullKeys() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(null, "Black");
            put(null, "Yellow");
            put(null, "Green");
        }};

        //and
        ImmutableMap immutableMap = ImmutableMap.from(map);

        //when then
        assertEquals("Actual map value different from the expected one", "Red", immutableMap.get(1));
        assertEquals("Actual map value different from the expected one", "Green", immutableMap.get(null));

        //and
        assertEquals("Actual collection size different from the expected one", map.size(), immutableMap.size());
        assertEquals("Actual collection size different from the expected one", 2, immutableMap.size());
    }

    @Test
    public void shouldCreateImmutableMap_fromMapWithSizeGreaterThanDefaultInitialHashtableSize() {
        //given
        Map<Integer, String> map = new HashMap<>();
        IntStream.range(0, ImmutableMap.DEFAULT_INITIAL_CAPACITY + 4).forEach(i -> map.put(i, String.valueOf(i)));

        //and
        ImmutableMap immutableMap = ImmutableMap.from(map);

        //when then
        assertEquals("Actual collection size different from the expected one", map.size(), immutableMap.size());

        //and
        assertEquals("Actual map value different from the expected one", "0", immutableMap.get(0));
        assertEquals("Actual map value different from the expected one", "19", immutableMap.get(19));
        assertNull("There is no value for the given key", immutableMap.get(20));
    }

    @Test
    public void shouldIndicateIfCollectionIsEmpty() {
        assertEquals("Collection should NOT be empty", false, immutableMap.isEmpty());
        assertEquals("Collection should be empty", true, ImmutableMap.from(Collections.emptyMap()).isEmpty());
    }

    @Test
    public void shouldIndicateIfMapContainsGivenKey() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(null, "Yellow");
            put(2, "Green");
        }};

        //and
        ImmutableMap immutableMap = ImmutableMap.from(map);

        //when then
        assertTrue("Map should contain the given key", immutableMap.containsKey(1));
        assertTrue("Map should contain the given key", immutableMap.containsKey(null));
        assertTrue("Map should contain the given key", immutableMap.containsKey(2));
        assertFalse("Map should NOT contain the given key", immutableMap.containsKey(77));
    }

    @Test
    public void shouldNotAllowPuttingNewElements() {
        try {
            immutableMap.put(1, "Pink");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableMap.putAll(Collections.emptyMap());
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableMap.putIfAbsent(4, "Black");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
    }

    @Test
    public void shouldNotAllowComputingElements() {
        try {
            immutableMap.compute(1, (integer, s) -> null);
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableMap.computeIfAbsent(1, integer -> null);
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableMap.computeIfPresent(2, (integer, s) -> null);
            fail();
        } catch (UnsupportedOperationException expected) {
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowClearingCollection() {
        immutableMap.clear();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowMergingElements() {
        immutableMap.merge(1, "Red", (s, s2) -> null);
    }

    @Test
    public void shouldNotAllowReplacingElements() {
        try {
            immutableMap.replace(1, "Red", "Pink");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableMap.replace(2, "Navy");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableMap.replaceAll((integer, s) -> null);
            fail();
        } catch (UnsupportedOperationException expected) {
        }
    }

    @Test
    public void shouldNotAllowRemovingElements() {
        try {
            immutableMap.remove(1);
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableMap.remove(2, "Yellow");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
    }

    @Test
    public void shouldReturnProperValueForTheGivenKey_integersKeys() {
        assertEquals("Actual map value different from the expected one", "Red", immutableMap.get(1));
        assertEquals("Actual map value different from the expected one", "Yellow", immutableMap.get(2));
        assertEquals("Actual map value different from the expected one", "Green", immutableMap.get(3));
        assertNull("There should be no value for the given key", immutableMap.get(4));
        assertNull("There should be no value for the given key", immutableMap.get(null));
    }

    @Test
    public void shouldReturnProperValueForTheGivenKey_stringsKeys() {
        //given
        Map<String, String> map = new HashMap<String, String>(0) {{
            put("ONE", "Red");
            put("TWO", "Yellow");
            put("THREE", "Green");
        }};

        //and
        ImmutableMap<String, String> immutableMap = ImmutableMap.from(map);

        //when then
        assertEquals("Actual map value different from the expected one", "Red", immutableMap.get("ONE"));
        assertEquals("Actual map value different from the expected one", "Yellow", immutableMap.get("TWO"));
        assertEquals("Actual map value different from the expected one", "Green", immutableMap.get("THREE"));
        assertNull("There should be no value for the given key", immutableMap.get("NON_EXISTING_KEY"));
        assertNull("There should be no value for the given key", immutableMap.get(null));
    }

    @Test
    public void shouldReturnProperValueForTheGivenKey_mixedHashCodeKeys() {
        //given
        Map<Object, String> map = new HashMap<>();
        map.put("ONE", "Red");
        map.put("TWO", "Yellow");
        map.put("THREE", "Green");
        map.put("FOUR", null);
        map.put("FOUR".hashCode(), "SameAsFour");
        map.put(String.valueOf("ONE".hashCode()), "SameAsRed");
        map.put("ONE".hashCode(), "SameAsRed");
        map.put("ONE".hashCode(), "SameAsRed");
        map.put("THREE".hashCode(), "SameAsGreen");
        map.put(null, "Black");
        map.put(null, null);

        //and
        ImmutableMap<Object, String> immutableMap = ImmutableMap.from(map);

        //when then
        assertEquals("Actual map value different from the expected one", "Red", immutableMap.get("ONE"));
        assertEquals("Actual map value different from the expected one", "Yellow", immutableMap.get("TWO"));
        assertEquals("Actual map value different from the expected one", "Green", immutableMap.get("THREE"));
        assertNull("Actual map value different from the expected one", immutableMap.get("FOUR"));
        assertEquals("Actual map value different from the expected one", "SameAsFour", immutableMap.get("FOUR".hashCode()));
        assertEquals("Actual map value different from the expected one", "SameAsRed", immutableMap.get(String.valueOf("ONE".hashCode())));
        assertEquals("Actual map value different from the expected one", "SameAsRed", immutableMap.get("ONE".hashCode()));
        assertNull("There should be no value for the given key", immutableMap.get("TWO".hashCode()));
        assertEquals("Actual map value different from the expected one", "SameAsGreen", immutableMap.get("THREE".hashCode()));
        assertNull("Actual map value different from the expected one", immutableMap.get(null));

        //and
        assertEquals("Actual collection size different from the expected one", map.size(), immutableMap.size());
        assertEquals("Actual collection size different from the expected one", 9, immutableMap.size());
    }

    @Test
    public void shouldReturnProperValueForTheGivenKey_theSameHashCodeKeys() {
        //given
        List<String> zeroHashCodeStrings = Arrays.asList("pollinating sandboxes", "amusement & hemophilias", "schoolworks = perversive", "electrolysissweeteners.net");

        //and
        Map<String, String> map = IntStream.range(0, zeroHashCodeStrings.size())
                .boxed()
                .collect(Collectors.toMap(zeroHashCodeStrings::get, String::valueOf));

        //when
        zeroHashCodeStrings.forEach(i -> assertThat("Hashcode should be equal zero", i.hashCode(), is(0)));

        //and
        ImmutableMap<Object, String> immutableMap = ImmutableMap.from(map);

        //then
        assertEquals("Actual map value different from the expected one", "0", immutableMap.get("pollinating sandboxes"));
        assertEquals("Actual map value different from the expected one", "1", immutableMap.get("amusement & hemophilias"));
        assertEquals("Actual map value different from the expected one", "3", immutableMap.get("electrolysissweeteners.net"));
        assertEquals("Actual map value different from the expected one", "2", immutableMap.get("schoolworks = perversive"));

        //and
        assertEquals("Actual collection size different from the expected one", map.size(), immutableMap.size());
    }

    @Test
    public void shouldReturnMapValuesProperly() {
        assertTrue("Collections should be equals", CollectionUtils.isEqualCollection(immutableMap.values(), Arrays.asList("Red", "Yellow", "Green")));
    }

    @Test
    public void shouldIndicateIfMapContainsGivenValue() {
        assertTrue("Collection should contain given value", immutableMap.containsValue("Yellow"));
        assertFalse("Collection should NOT contain given value", immutableMap.containsValue("Navy"));
        assertFalse("Collection should NOT contain given value", immutableMap.containsValue(null));
    }

    @Test
    public void shouldReturnMapKeysProperly() {
        assertTrue("Collections should be equals", CollectionUtils.isEqualCollection(immutableMap.keySet(), Arrays.asList(1, 2, 3)));
        assertTrue("Key set should contain given element", immutableMap.keySet().contains(1));
        assertTrue("Key set should contain given collection", immutableMap.keySet().containsAll(Arrays.asList(1, 3)));
        assertTrue("Key set should contain given collection", CollectionUtils.isEqualCollection(immutableMap.keySet().asList(), Arrays.asList(1, 2, 3)));
    }

    @Test
    public void shouldReturnMapEntriesProperly() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "Green");
        }};

        Set<Map.Entry<Integer, String>> elementsToBeCompared = map.entrySet();

        //when then
        assertTrue("Key set should contain given collection", CollectionUtils.isEqualCollection(immutableMap.entrySet(), elementsToBeCompared));
    }

    @Test
    public void shouldCompareProperlyTwoEntrySets() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "Green");
        }};

        //and
        Set<Map.Entry<Integer, String>> entrySetToBeCompared = map.entrySet();

        //when then
        assertEquals("Entry sets should be equal", true, immutableMap.entrySet().equals(entrySetToBeCompared));
        assertTrue("Entry sets MUST have the same hashcode", immutableMap.entrySet().hashCode() == entrySetToBeCompared.hashCode());

        //given
        map.put(4, "Black");
        entrySetToBeCompared = map.entrySet();

        //when then
        assertEquals("Entry sets should NOT be equal", false, immutableMap.entrySet().equals(entrySetToBeCompared));
        assertTrue("Entry sets better should NOT have the same hashcode", immutableMap.entrySet().hashCode() != entrySetToBeCompared.hashCode());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowChangingMapEntryValue() {
        immutableMap.entrySet().iterator().next().setValue("Impossible");
    }

    @Test
    public void shouldCompareProperlyTwoMapEntries() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
        }};
        //and
        Map.Entry<Integer, String> entryToBeCompared = map.entrySet().iterator().next();
        Map.Entry<Integer, String> immutableMapEntry = immutableMap.entrySet().iterator().next();

        //when then
        assertThat("Entries should be equal", immutableMapEntry.equals(entryToBeCompared), is(true));
        assertThat("Entries MUST have the same hashcode", map.entrySet().iterator().next().hashCode() == immutableMap.entrySet().iterator().next().hashCode(), is(true));

        //when
        map.put(1, null);

        //then
        assertThat("Entries should NOT be equal", immutableMapEntry.equals(map.entrySet().iterator().next()), is(false));
        assertThat("Entries better should NOT have the same hashcode", immutableMapEntry.hashCode() == map.entrySet().iterator().next().hashCode(), is(false));

        //when
        map.put(1, "Black");

        //then
        assertThat("Entries should NOT be equal", immutableMapEntry.equals(map.entrySet().iterator().next()), is(false));
        assertThat("Entries better should NOT have the same hashcode", immutableMapEntry.hashCode() == map.entrySet().iterator().next().hashCode(), is(false));

        //when
        map.remove(1);
        map.put(2, "Yellow");

        //then
        assertThat("Entries should NOT be equal", immutableMapEntry.equals(map.entrySet().iterator().next()), is(false));
        assertThat("Entries better should NOT have the same hashcode", immutableMapEntry.hashCode() == map.entrySet().iterator().next().hashCode(), is(false));

        //when
        map.remove(2);
        map.put(null, "Black");

        //then
        assertThat("Entries should NOT be equal", immutableMapEntry.equals(map.entrySet().iterator().next()), is(false));
        assertThat("Entries better should NOT have the same hashcode", immutableMapEntry.hashCode() == map.entrySet().iterator().next().hashCode(), is(false));

        //when
        map.put(null, null);

        //then
        assertThat("Entries should NOT be equal", immutableMapEntry.equals(map.entrySet().iterator().next()), is(false));
        assertThat("Entries better should NOT have the same hashcode", immutableMapEntry.hashCode() == map.entrySet().iterator().next().hashCode(), is(false));
    }

    @Test
    public void shouldCompareProperlyTwoMapEntries_differentTypes() {
        //given
        Map.Entry<Integer, String> immutableMapEntry = immutableMap.entrySet().iterator().next();

        //when then
        assertThat("Entries should NOT be equal", immutableMapEntry.equals("DifferentTypeObject"), is(false));
        assertThat("Entries better should NOT have the same hashcode", immutableMapEntry.hashCode() == "DifferentTypeObject".hashCode(), is(false));

        //and
        assertThat("Entries should NOT be equal", immutableMap.entrySet().iterator().next().equals(null), is(false));
    }

    @Test
    public void shouldIndicateIfMapEntrySetContainsGivenValue() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "NonExistingEntry");
        }};

        //and
        Set<Map.Entry<Integer, String>> immutableMapEntrySet = immutableMap.entrySet();

        assertTrue("Entry set should contain given element", immutableMapEntrySet.contains(map.entrySet().iterator().next()));
        assertFalse("Entry set should NOT contain given element", immutableMapEntrySet.contains(new Object()));
        assertFalse("Entry set should NOT contain given element", immutableMapEntrySet.contains(null));

        //given
        map.put(1, "NonExistingEntry");

        //when then
        assertFalse("Entry set should NOT contain given element", immutableMap.entrySet().contains(map.entrySet().iterator().next()));

        //given
        map.put(1, null);

        //when then
        assertFalse("Entry set should NOT contain given element", immutableMap.entrySet().contains(map.entrySet().iterator().next()));
    }

    @Test
    public void shouldReturnMapEntriesAsListProperly() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "Green");
        }};

        //and
        Set<Map.Entry<Integer, String>> elementsToBeCompared = map.entrySet();

        //when
        List<Map.Entry<Integer, String>> immutableEntrySetList = immutableMap.entrySet().asList();

        //then
        assertThat("Entry set list should NOT be empty", immutableEntrySetList.isEmpty(), is(false));

        //and
        assertTrue("Entry set should contain given collection", CollectionUtils.isEqualCollection(immutableEntrySetList, elementsToBeCompared));

        //and
        assertTrue("Entry set list should contain given element", immutableEntrySetList.contains(map.entrySet().iterator().next()));
        assertFalse("Entry set list should NOT contain given element", immutableEntrySetList.contains(new Object()));
        assertFalse("Entry set list should NOT contain given element", immutableEntrySetList.contains(null));

        //and
        assertTrue("Entry set list should contain given element", immutableEntrySetList.containsAll(Collections.singletonList(map.entrySet().iterator().next())));
    }

    @Test
    public void shouldIterateOverTheMapKeySetProperly() {
        //given
        List<Integer> elementsToBeCompared = Arrays.asList(1, 2, 3);

        //when then
        forEach(immutableMap.keySet(), (el, i, n) -> assertEquals("Actual collection element is different from the expected one", elementsToBeCompared.get(i), el));
    }

    @Test
    public void shouldReturnMapKeySetAsListProperly() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "Green");
        }};

        //and
        Set<Integer> elementsToBeCompared = map.keySet();

        //when
        List<Integer> immutableKeySetList = immutableMap.keySet().asList();

        //then
        assertThat("Key set list should NOT be empty", immutableKeySetList.isEmpty(), is(false));

        //and
        assertTrue("Key set should contain given collection", CollectionUtils.isEqualCollection(immutableKeySetList, elementsToBeCompared));

        //and
        assertTrue("Key set list should contain given element", immutableKeySetList.contains(map.keySet().iterator().next()));
        assertFalse("Key set list should NOT contain given element", immutableKeySetList.contains(new Object()));
        assertFalse("Key set list should NOT contain given element", immutableKeySetList.contains(null));

        //and
        assertTrue("Key set list should contain given element", immutableKeySetList.containsAll(Collections.singletonList(map.keySet().iterator().next())));
    }

    @Test
    public void shouldCompareProperlyTwoKeySets() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "Green");
        }};

        //and
        Set<Integer> keySetToBeCompared = map.keySet();

        //when then
        assertEquals("Key sets should be equal", true, immutableMap.keySet().equals(keySetToBeCompared));
        assertTrue("Key sets MUST have the same hashcode", immutableMap.keySet().hashCode() == keySetToBeCompared.hashCode());

        //given
        map.put(4, "Black");
        keySetToBeCompared = map.keySet();

        //when then
        assertEquals("Key sets should NOT be equal", false, immutableMap.keySet().equals(keySetToBeCompared));
        assertTrue("Key sets better should NOT have the same hashcode", immutableMap.keySet().hashCode() != keySetToBeCompared.hashCode());
    }

    @Test
    public void shouldCompareProperlyTwoImmutableMaps() {
        //given
        Map<Integer, String> map = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "Green");
        }};

        //and
        ImmutableMap<Integer, String> sameMapToCompare = ImmutableMap.from(map);

        //when then
        assertEquals("Maps should be equal", true, immutableMap.equals(sameMapToCompare));
        assertTrue("Maps MUST have the same hashcode", sameMapToCompare.hashCode() == immutableMap.hashCode());

        //given
        map.put(1, "Black");
        ImmutableMap<Integer, String> differentMapToCompare = ImmutableMap.from(map);

        //when then
        assertEquals("Maps should NOT be equal", false, immutableMap.equals(differentMapToCompare));
        assertTrue("Maps better should NOT have the same hashcode", differentMapToCompare.hashCode() != immutableMap.hashCode());
    }

    @Test
    public void shouldCompareProperlyTwoMaps() {
        //given
        Map<Integer, String> mapToCompare = new HashMap<Integer, String>() {{
            put(1, "Red");
            put(2, "Yellow");
            put(3, "Green");
        }};

        //when then
        assertEquals("Maps should be equal", true, immutableMap.equals(mapToCompare));
        assertTrue("Maps MUST have the same hashcode", mapToCompare.hashCode() == immutableMap.hashCode());

        //and when
        mapToCompare.put(1, "Black");

        //then
        assertEquals("Maps should NOT be equal", false, immutableMap.equals(mapToCompare));
        assertTrue("Maps better should NOT have the same hashcode", mapToCompare.hashCode() != immutableMap.hashCode());
    }

    @Test
    public void shouldCompareProperly_nullObject() {
        assertEquals("Objects should NOT be equal", false, immutableMap.equals(null));
    }

    @Test
    public void shouldCompareProperly_nonAMap() {
        assertEquals("Objects should NOT be equal", false, immutableMap.equals(new Object()));
    }

    @Test
    public void shouldCompareProperly_sameObject() {
        assertEquals("Objects should be equal", true, immutableMap.equals(immutableMap));
    }

}