package com.example.immutability.collections;

import java.util.Collection;

/**
 * Created by mjzi on 2016-06-17
 */
public class TestUtils {

    @FunctionalInterface
    public interface LoopWithIndexAndSizeConsumer<T> {
        void accept(T t, int i, int n);
    }

    public static <T> void forEach(Collection<T> collection,
                                   LoopWithIndexAndSizeConsumer<T> consumer) {
        int index = 0;
        for (T object : collection) {
            consumer.accept(object, index++, collection.size());
        }
    }

}
