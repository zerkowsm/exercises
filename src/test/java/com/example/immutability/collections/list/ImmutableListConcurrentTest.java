package com.example.immutability.collections.list;

import com.example.immutability.collections.ImmutableCollectionConcurrentBaseTest;
import junitparams.Parameters;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

/**
 * Some more concurrency tests for {@link ImmutableList} class.
 * <p>
 * Created by mjzi on 2016-06-20
 */
public class ImmutableListConcurrentTest<T> extends ImmutableCollectionConcurrentBaseTest<T> {

    @Parameters(method = "iterationOverCollectionWithAddingTryTestDataLoad")
    @Test
    public void shouldIterateConcurrentlyOverTheCollection_tryingToAddElement(int threadPoolSize, List<T> list, boolean isListImmutable) throws InterruptedException {
        //given
        AtomicInteger errorsCounter = new AtomicInteger();

        //and
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        List<Callable<T>> processors = new ArrayList<>();

        for (int i = 0; i < threadPoolSize; i++) {
            Callable<T> processor = new ListIteratorWithAddingTryProcessor<>(list, ThreadLocalRandom.current().nextInt(0, list.size()));
            processors.add(processor);
        }

        //when
        List<Future<T>> results = executor.invokeAll(processors);

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.SECONDS);

        //then
        for (Future<T> result : results) {
            try {
                result.get();
            } catch (ExecutionException expected) {
                if (isListImmutable) {
                    assertNotEquals("Processor can NOT throw java.util.ConcurrentModificationException", "java.util.ConcurrentModificationException", expected.getMessage());
                    assertEquals("Processor can throw only java.lang.UnsupportedOperationException", "java.lang.UnsupportedOperationException", expected.getMessage());
                } else {
                    assertEquals("Processor can throw only java.util.ConcurrentModificationException", "java.util.ConcurrentModificationException", expected.getMessage());
                    assertNotEquals("Processor can NOT throw java.lang.UnsupportedOperationException", "java.lang.UnsupportedOperationException", expected.getMessage());
                }
                errorsCounter.getAndIncrement();
            }
        }

        //and
        if (isListImmutable) {
            assertEquals("Number of exception being thrown must be equals to number of processors", threadPoolSize, errorsCounter.get());
        } else if (threadPoolSize == 1) {
            assertEquals("Number of exception being thrown must be equal to 0 for one thread", 0, errorsCounter.get());
        } else {
            assertTrue("Number of exception being thrown should be close to number of threads but might be a bit less (~298, 299 depending on your hardware and collection iteration time) then exact thread number", 0 < errorsCounter.get() && errorsCounter.get() <= threadPoolSize);
        }
    }

    private Object iterationOverCollectionWithAddingTryTestDataLoad() {
        return new Object[]{
                new Object[]{1, ImmutableList.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), true},
                new Object[]{300, ImmutableList.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), true},
                new Object[]{1, new ArrayList<>(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), false},
                new Object[]{300, new ArrayList<>(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), false}
        };
    }

    protected class ListIteratorWithAddingTryProcessor<E> implements Callable<E> {

        private final List<E> list;
        private int elementToBeRemoved;

        ListIteratorWithAddingTryProcessor(List<E> list, int elementToBeRemoved) {
            this.list = list;
            this.elementToBeRemoved = elementToBeRemoved;
        }

        @SuppressWarnings("unchecked")
        @Override
        public E call() throws Exception {
            ListIterator iterator = list.listIterator();
            int loopCounter = 0;
            while (iterator.hasNext()) {
                iterator.next();
                Thread.sleep(100);
                if (loopCounter++ == elementToBeRemoved) {
                    iterator.add("Deep Blue");
                }
            }
            return null;
        }
    }

}
