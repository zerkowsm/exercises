package com.example.immutability.collections.list;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static com.example.immutability.collections.TestUtils.forEach;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by mjzi on 2016-06-15
 */
public class ImmutableListTest {

    private static BaseImmutableList<String> immutableList;

    @BeforeClass
    public static void setUp() throws Exception {
        immutableList = ImmutableList.from(Arrays.asList("Red", "Yellow", "Green"));
    }

    @AfterClass
    public static void tearDown() {
        assertEquals("Actual collection size initiated before the test suite is different from the one got after the test suite execution", 3, immutableList.size());
    }

    @Test
    public void shouldNotChangeAfterCreated_fromLeavingList() {
        //given
        List<String> originalList = new ArrayList<>(Arrays.asList("Red", "Yellow", "Green"));

        //and
        BaseImmutableList<String> immutableList = ImmutableList.from(originalList);

        //when
        originalList.add("Black");
        originalList.remove(0);
        originalList.remove(1);
        Collections.sort(originalList);

        //then
        assertEquals("Actual collection size is different from the expected one", 3, immutableList.size());
        assertEquals("Actual collection element is different from the expected one", "Red", immutableList.get(0));
        assertEquals("Actual collection element is different from the expected one", "Yellow", immutableList.get(1));
        assertEquals("Actual collection element is different from the expected one", "Green", immutableList.get(2));
    }

    @Test
    public void shouldCreateImmutableSet_fromLeavingObjectArray() {
        //given
        Object[] array = Arrays.asList("Red", "Yellow", null, "Yellow", "Red", null, "Green").toArray();

        //and
        BaseImmutableList immutableList = ImmutableList.from(array);

        //when then
        assertEquals("Actual collection size is different from the expected one", 7, immutableList.size());

        //given
        List<String> elementsToBeCompared = Arrays.asList("Red", "Yellow", null, "Yellow", "Red", null, "Green");

        //and
        array[0] = "Black";

        //when then
        forEach(immutableList, (el, i, n) -> assertEquals("Actual collection element is different from the expected one", elementsToBeCompared.get(i), el));
    }

    @Test
    public void shouldCreateImmutableList_fromSet() {
        //given
        Set<String> set = new HashSet<>(Arrays.asList("Red", "Yellow", "Yellow", "Red", "Green"));

        //when then
        assertEquals("Actual collection size is different from the expected one", 3, ImmutableList.from(set).size());
    }

    @Test
    public void shouldCreateImmutableList_fromListWithDuplicates() {
        //given
        List<String> listWithDuplicates = new ArrayList<>(Arrays.asList("Red", "Yellow", "Yellow", "Red", "Green"));

        //when then
        assertEquals("Actual collection size is different from the expected one", 5, ImmutableList.from(listWithDuplicates).size());
    }

    @Test
    public void shouldIterateOverTheCollection() {
        //given
        List<String> elementsToBeCompared = Arrays.asList("Red", "Yellow", "Green");

        //when then
        forEach(immutableList, (el, i, n) -> assertEquals("Actual collection element is different from the expected one", elementsToBeCompared.get(i), el));
    }

    @Test
    public void shouldIndicateIfCollectionContainsGivenElement() {
        assertEquals("Actual result is different from the expected one", true, immutableList.contains("Yellow"));
        assertEquals("Actual result is different from the expected one", false, immutableList.contains("Orange"));
    }

    @Test
    public void shouldReturnIndexProperly() {
        assertEquals("Actual collection element index is different from the expected one", 1, immutableList.indexOf("Yellow"));
        assertEquals("Actual collection element index is different from the expected one", -1, immutableList.indexOf("Orange"));
    }

    @Test
    public void shouldReturnLastIndexProperly() {
        //when then
        assertEquals("Actual collection element last index is different from the expected one", 2, immutableList.lastIndexOf("Green"));

        //given
        BaseImmutableList<String> immutableList = ImmutableList.from(Arrays.asList("Red", "Yellow", "Green", "Green"));

        //when then
        assertEquals("Actual collection element last index is different from the expected one", 3, immutableList.lastIndexOf("Green"));
        assertEquals("Actual collection element last index is different from the expected one", -1, immutableList.lastIndexOf("Orange"));
    }

    @Test
    public void shouldNotAllowAddingNewElement() {
        try {
            immutableList.add("Blue");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableList.add(1, "Blue");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableList.addAll(Arrays.asList("Ping", "Navy"));
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableList.addAll(1, Arrays.asList("Ping", "Navy"));
            fail();
        } catch (UnsupportedOperationException expected) {
        }
    }

    @Test
    public void shouldNotAllowRemovingElement() {
        try {
            immutableList.remove(0);
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableList.remove("Green");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableList.removeAll(Arrays.asList("Yellow", "Green"));
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableList.removeIf(s -> true);
            fail();
        } catch (UnsupportedOperationException expected) {
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowClearingCollection() {
        immutableList.clear();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowReplacingElement() {
        immutableList.replaceAll(s -> null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowRetainingAnyElement() {
        immutableList.retainAll(Arrays.asList("Yellow", "Green"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowSetting() {
        immutableList.set(0, "Grey");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowSorting() {
        immutableList.sort(String::compareToIgnoreCase);
    }

    @Test
    public void shouldIndicateIfCollectionIsEmpty() {
        assertEquals("Collection should NOT be empty", false, immutableList.isEmpty());
        assertEquals("Collection should be empty", true, ImmutableList.from(Collections.emptyList()).isEmpty());
        assertEquals("Collection should be empty", true, ImmutableList.from(Collections.emptySet()).isEmpty());
    }

    @Test
    public void shouldCompareProperlyTwoImmutableLists() {
        //given
        BaseImmutableList<String> sameListToCompare = ImmutableList.from(Arrays.asList("Red", "Yellow", "Green"));

        //and
        BaseImmutableList<String> differentListToCompare = ImmutableList.from(Arrays.asList("Red", "Black", "Green"));

        //when then
        assertEquals("Lists should be equal", true, sameListToCompare.equals(immutableList));
        assertTrue("Lists MUST have the same hashcode", sameListToCompare.hashCode() == immutableList.hashCode());

        //and
        assertEquals("Lists should NOT be equal", false, differentListToCompare.equals(immutableList));
        assertTrue("Lists better should NOT have the same hashcode", differentListToCompare.hashCode() != immutableList.hashCode());

        //given
        BaseImmutableList<String> subListToCompare = immutableList.subList(0, 3);

        //when then
        assertEquals("Lists should be equal", true, subListToCompare.equals(immutableList));
        assertTrue("Lists MUST have the same hashcode", subListToCompare.hashCode() == immutableList.hashCode());
    }

    @Test
    public void shouldCompareProperlyTwoLists() {
        //given
        List<String> listToCompare = new ArrayList<String>() {{
            add("Red");
            add("Yellow");
            add("Green");
        }};

        //and
        BaseImmutableList immutableList = ImmutableList.from(listToCompare);

        //when then
        assertEquals("Lists should be equal", true, listToCompare.equals(immutableList));
        assertTrue("Lists MUST have the same hashcode", listToCompare.hashCode() == immutableList.hashCode());

        //and when
        listToCompare.add(2, "Black");

        //then
        assertEquals("Lists should NOT be equal", false, listToCompare.equals(immutableList));
        assertTrue("Lists better should NOT have the same hashcode", listToCompare.hashCode() != immutableList.hashCode());
    }

    @Test
    public void shouldCompareProperlyTwoImmutableLists_nullElements() {
        //given
        BaseImmutableList<String> immutableList = ImmutableList.from(Arrays.asList("Red", "Yellow", null, "Green", null));

        //and
        BaseImmutableList<String> sameListToCompare = ImmutableList.from(Arrays.asList("Red", "Yellow", null, "Green", null));
        BaseImmutableList<String> differentListToCompare = ImmutableList.from(Arrays.asList("Red", "Black", "Green"));

        //when then
        assertEquals("Lists should be equal", true, sameListToCompare.equals(immutableList));
        assertTrue("Lists MUST have the same hashcode", sameListToCompare.hashCode() == immutableList.hashCode());

        //and
        assertEquals("Lists should NOT be equal", false, differentListToCompare.equals(immutableList));
        assertTrue("Lists better should NOT have the same hashcode", differentListToCompare.hashCode() != immutableList.hashCode());

        //given
        BaseImmutableList<String> subListToCompare = immutableList.subList(0, 5);

        //when then
        assertEquals("Lists should be equal", true, subListToCompare.equals(immutableList));
        assertTrue("Lists MUST have the same hashcode", subListToCompare.hashCode() == immutableList.hashCode());
    }

    @Test
    public void shouldCompareProperlyTwoImmutableLists_wrongElementsOrder() {
        //given
        BaseImmutableList<String> differentListToCompare = ImmutableList.from(Arrays.asList("Red", "Green", "Yellow"));

        //when then
        assertEquals("Lists should NOT be equal", false, differentListToCompare.equals(immutableList));
        assertTrue("Lists better should NOT have the same hashcode", differentListToCompare.hashCode() != immutableList.hashCode());
    }

    @Test
    public void shouldCompareProperly_nullObject() {
        assertEquals("Objects should NOT be equal", false, immutableList.equals(null));
    }

    @Test
    public void shouldCompareProperly_nonAList() {
        assertEquals("Objects should NOT be equal", false, immutableList.equals(new Object()));

        //given
        String someNonListObjectWithEqualsAndHashCode = "someNonListObject";

        //when then
        assertEquals("Objects should NOT be equal", false, immutableList.equals(someNonListObjectWithEqualsAndHashCode));

        //and
        assertTrue("Objects better should NOT have the same hashcode", someNonListObjectWithEqualsAndHashCode.hashCode() != immutableList.hashCode());
    }

    @Test
    public void shouldNotReturnSubListOfImmutableList_wrongParametersRange() {
        try {
            immutableList.subList(1, 4);
            fail();
        } catch (IllegalArgumentException expected) {
        }
        try {
            immutableList.subList(3, 1);
            fail();
        } catch (IllegalArgumentException expected) {
        }
        try {
            immutableList.subList(-11, 1);
            fail();
        } catch (IllegalArgumentException expected) {
        }
    }

    @Test
    public void shouldReturnSubListOfImmutableList() {
        //when
        BaseImmutableList<String> subList = immutableList.subList(1, 3);

        //then
        assertEquals("Actual collection size is different from the expected one", 2, subList.size());
        assertEquals("Actual collection size is different from the expected one", 3, immutableList.size());
        assertEquals("Actual collection element is different from the expected one", "Yellow", subList.get(0));
        assertEquals("Actual collection element is different from the expected one", "Green", subList.get(1));
        assertEquals("Actual result is different from the expected one", true, subList.contains("Yellow"));
        assertEquals("Actual result is different from the expected one", false, subList.contains("Orange"));
        assertEquals("Actual collection element index is different from the expected one", 0, subList.indexOf("Yellow"));
        assertEquals("Actual collection element index is different from the expected one", -1, subList.indexOf("Orange"));
        assertEquals("Actual collection element last index is different from the expected one", 1, subList.lastIndexOf("Green"));
        assertEquals("Actual collection element last index is different from the expected one", -1, subList.lastIndexOf("Orange"));

        //when
        BaseImmutableList<String> subListSubList = subList.subList(1, 2);

        //then
        assertEquals("Actual collection size is different from the expected one", 1, subListSubList.size());
        assertEquals("Actual collection size is different from the expected one", 3, immutableList.size());
        assertEquals("Actual collection element is different from the expected one", "Green", subListSubList.get(0));
    }

}