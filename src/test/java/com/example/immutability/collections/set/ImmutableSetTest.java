package com.example.immutability.collections.set;

import com.example.immutability.collections.list.BaseImmutableList;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static com.example.immutability.collections.TestUtils.forEach;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Created by mjzi on 2016-06-15
 */
public class ImmutableSetTest {

    private static BaseImmutableSet<String> immutableSet;

    @BeforeClass
    public static void setUp() throws Exception {
        immutableSet = ImmutableSet.from(Arrays.asList("Red", "Yellow", "Yellow", "Red", "Green"));
    }

    @AfterClass
    public static void tearDown() {
        assertEquals("Actual collection size initiated before the test suite is different from the one got after the test suite execution", 3, immutableSet.size());
    }

    @Test
    public void shouldCreateImmutableSet_fromSet() {
        //given
        Set<String> set = new HashSet<>(Arrays.asList("Red", "Yellow", "Yellow", "Red", "Green"));

        //when then
        assertEquals("Actual collection size is different from the expected one", 3, ImmutableSet.from(set).size());
    }

    @Test
    public void shouldCreateImmutableSet_fromLeavingObjectArray() {
        //given
        Object[] array = Arrays.asList("Red", "Yellow", "Yellow", "Red", "Green").toArray();

        //and
        BaseImmutableSet immutableSet = ImmutableSet.from(array);

        //when then
        assertEquals("Actual collection size is different from the expected one", 3, immutableSet.size());

        //given
        List<String> elementsToBeCompared = Arrays.asList("Red", "Yellow", "Green");

        //and
        array[0] = "Black";

        //when then
        forEach(immutableSet, (el, i, n) -> assertEquals("Actual collection element is different from the expected one", elementsToBeCompared.get(i), el));
    }

    @Test
    public void shouldNotChangeAfterCreated_fromLeavingList() {
        //given
        List<String> originalList = new ArrayList<>(Arrays.asList("Red", "Yellow", "Green"));

        //and
        BaseImmutableSet<String> immutableSet = ImmutableSet.from(originalList);

        //when
        originalList.add("Black");
        originalList.remove(0);
        originalList.remove(1);
        Collections.sort(originalList);

        //then
        assertEquals("Actual collection size is different from the expected one", 3, immutableSet.size());
    }

    @Test
    public void shouldCreateImmutableSet_fromListAndArrayWithNullElements() {
        //given
        List<String> listWithNullElements = Arrays.asList("Red", "Yellow", null, "Yellow", "Red", null, "Green");

        //when then
        assertEquals("Actual collection size is different from the expected one", 4, ImmutableSet.from(listWithNullElements).size());

        //given
        Object[] array = Arrays.asList("Red", "Yellow", null, "Yellow", "Red", null, null, "Green").toArray();

        //when then
        assertEquals("Actual collection size is different from the expected one", 4, ImmutableSet.from(array).size());
    }

    @Test
    public void shouldIterateOverTheCollection() {
        //given
        List<String> elementsToBeCompared = Arrays.asList("Red", "Yellow", "Green");

        //when then
        forEach(immutableSet, (el, i, n) -> assertEquals("Actual collection element is different from the expected one", elementsToBeCompared.get(i), el));
    }

    @Test
    public void shouldNotAllowAddingNewElement() {
        try {
            immutableSet.add("Blue");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableSet.addAll(Arrays.asList("Ping", "Navy"));
            fail();
        } catch (UnsupportedOperationException expected) {
        }
    }

    @Test
    public void shouldNotAllowRemovingElement() {
        try {
            immutableSet.remove("Green");
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableSet.removeAll(Arrays.asList("Yellow", "Green"));
            fail();
        } catch (UnsupportedOperationException expected) {
        }
        try {
            immutableSet.removeIf(s -> true);
            fail();
        } catch (UnsupportedOperationException expected) {
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowClearingCollection() {
        immutableSet.clear();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotAllowRetainingAnyElement() {
        immutableSet.retainAll(Arrays.asList("Yellow", "Green"));
    }

    @Test
    public void shouldIndicateIfCollectionContainsGivenElement() {
        assertEquals("Actual result is different from the expected one", true, immutableSet.contains("Yellow"));
        assertEquals("Actual result is different from the expected one", false, immutableSet.contains("Orange"));
    }

    @Test
    public void shouldIndicateIfCollectionIsEmpty() {
        assertEquals("Collection should NOT be empty", false, immutableSet.isEmpty());
        assertEquals("Collection should be empty", true, ImmutableSet.from(Collections.emptyList()).isEmpty());
        assertEquals("Collection should be empty", true, ImmutableSet.from(Collections.emptySet()).isEmpty());
    }

    @Test
    public void shouldCompareProperlyTwoImmutableSets() {
        //given
        BaseImmutableSet<String> sameSetToCompare = ImmutableSet.from(Arrays.asList("Red", "Yellow", "Yellow", "Green"));

        //and
        BaseImmutableSet<String> differentSetToCompare = ImmutableSet.from(Arrays.asList("Red", "Black", "Green"));

        //when then
        assertEquals("Sets should be equal", true, sameSetToCompare.equals(immutableSet));
        assertTrue("Sets MUST have the same hashcode", sameSetToCompare.hashCode() == immutableSet.hashCode());

        //and
        assertEquals("Sets should NOT be equal", false, differentSetToCompare.equals(immutableSet));
        assertTrue("Sets better should NOT have the same hashcode", differentSetToCompare.hashCode() != immutableSet.hashCode());
    }

    @Test
    public void shouldCompareProperlyTwoImmutableSets_nullElements() {
        //given
        BaseImmutableSet immutableSet = ImmutableSet.from(Arrays.asList("Red", "Yellow", null, "Green", null));

        //and
        BaseImmutableSet<String> sameSetToCompare = ImmutableSet.from(Arrays.asList("Red", "Yellow", null, "Green", null));
        BaseImmutableSet<String> differentListToCompare = ImmutableSet.from(Arrays.asList("Red", "Black", "Green"));

        //when then
        assertEquals("Sets should be equal", true, sameSetToCompare.equals(immutableSet));
        assertTrue("Sets MUST have the same hashcode", sameSetToCompare.hashCode() == immutableSet.hashCode());

        //and
        assertEquals("Sets should NOT be equal", false, differentListToCompare.equals(immutableSet));
        assertTrue("Sets better should NOT have the same hashcode", differentListToCompare.hashCode() != immutableSet.hashCode());
    }

    @Test
    public void shouldCompareProperlyTwoImmutableLists_elementsOrderDoesNotMatter() {
        //given
        BaseImmutableSet<String> differentSetToCompare = ImmutableSet.from(Arrays.asList("Yellow", "Green", "Red", "Yellow", "Red"));

        //when then
        assertEquals("Sets should be equal", true, differentSetToCompare.equals(immutableSet));
        assertTrue("Sets MUST have the same hashcode", differentSetToCompare.hashCode() == immutableSet.hashCode());
    }

    @Test
    public void shouldCompareProperly_nullObject() {
        assertEquals("Objects should NOT be equal", false, immutableSet.equals(null));
    }

    @Test
    public void shouldCompareProperly_nonASet() {
        assertEquals("Objects should NOT be equal", false, immutableSet.equals(new Object()));
    }

    @Test
    public void shouldReturnImmutableSetAsImmutableListWithExactSameElements() {
        //when
        List<String> immutableList = immutableSet.asList();

        //then
        assertThat("Object should be an instance of immutable list", immutableList, is(instanceOf(BaseImmutableList.class)));
        assertThat("Immutable list's size should be the same as immutable set's size", immutableList.size(), is(immutableSet.size()));

        //and
        forEach(immutableSet, (el, i, n) -> assertEquals("Actual collection element is different from the expected one", immutableList.get(i), el));

        //when
        List sameImmutableList = immutableSet.asList();

        //then
        assertSame("Objects should be the same", immutableList, sameImmutableList);
    }

    @Test
    public void shouldReturnImmutableSetAsImmutableList() {
        //given
        List<String> elementsToBeCompared = Arrays.asList("Red", "Yellow", "Green");

        //when
        List<String> immutableSetAsList = immutableSet.asList();

        //then
        Assert.assertThat("List should NOT be empty", immutableSetAsList.isEmpty(), is(false));

        //and
        assertTrue("Key set should contain given collection", CollectionUtils.isEqualCollection(immutableSetAsList, elementsToBeCompared));

        //and
        assertTrue("List should contain given element", immutableSetAsList.contains(elementsToBeCompared.get(2)));
        assertFalse("List should NOT contain given element", immutableSetAsList.contains(new Object()));
        assertFalse("List should NOT contain given element", immutableSetAsList.contains(null));

        //and
        assertTrue("Entry set list should contain given element", immutableSetAsList.containsAll(Arrays.asList("Red", "Green")));
    }

    @Test
    public void shouldCompareProperlyImmutableSetsAsLists() {
        //given
        List<String> sameElementsToBeCompared = Arrays.asList("Red", "Yellow", "Green");

        //and
        List<String> differentElementsToBeCompared = Arrays.asList("Red", "Black", null, "Green");

        //when then
        assertEquals("Lists should be equal", true, immutableSet.asList().equals(sameElementsToBeCompared));
        assertTrue("Lists MUST have the same hashcode", immutableSet.asList().hashCode() == sameElementsToBeCompared.hashCode());

        //and
        assertEquals("Lists should NOT be equal", false, differentElementsToBeCompared.equals(immutableSet.asList()));
        assertTrue("Lists better should NOT have the same hashcode", differentElementsToBeCompared.hashCode() != immutableSet.asList().hashCode());
    }

    @Test
    public void shouldGetSubListOfSetAsListProperly() {
        //given
        List<String> elementsToBeCompared = Arrays.asList("Red", "Yellow");

        //when
        List<String> setAsListSubList = immutableSet.asList().subList(0, 2);

        //then
        assertEquals("Lists should be equal", true, setAsListSubList.equals(elementsToBeCompared));
        assertTrue("Lists MUST have the same hashcode", setAsListSubList.hashCode() == elementsToBeCompared.hashCode());
    }

}