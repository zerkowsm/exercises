package com.example.immutability.collections;

import com.example.immutability.collections.list.ImmutableList;
import com.example.immutability.collections.set.ImmutableSet;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.example.immutability.collections.TestUtils.forEach;
import static org.junit.Assert.*;

/**
 * Some concurrency tests for both {@link ImmutableList} and {@link ImmutableSet} classes.
 * <p>
 * Created by mjzi on 2016-06-20
 */
@RunWith(JUnitParamsRunner.class)
public class ImmutableCollectionConcurrentBaseTest<T> {

    @Parameters(method = "iterationOverCollectionTestDataLoad")
    @Test
    public void shouldIterateConcurrentlyOverTheCollection(int threadPoolSize, Collection<T> collection, List<T> elementsToBeCompared) throws InterruptedException {
        //given
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        List<Callable<T>> processors = new ArrayList<>();

        //when
        for (int i = 0; i < threadPoolSize; i++) {
            Callable<T> processor = new CollectionIteratorProcessor<>(collection, elementsToBeCompared);
            processors.add(processor);
        }

        //then
        executor.invokeAll(processors);

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.SECONDS);
    }

    private Object iterationOverCollectionTestDataLoad() {
        return new Object[]{
                new Object[]{1, ImmutableList.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")},
                new Object[]{300, ImmutableList.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")},
                new Object[]{1, ImmutableSet.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")},
                new Object[]{300, ImmutableSet.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")},
                new Object[]{1, new ArrayList<>(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")},
                new Object[]{300, new HashSet<>(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")}
        };
    }

    @Parameters(method = "iterationOverCollectionWithRemovalTryTestDataLoad")
    @Test
    public void shouldIterateConcurrentlyOverTheCollection_tryingToRemoveElement(int threadPoolSize, Collection<T> collection, boolean isCollectionImmutable) throws InterruptedException {
        //given
        AtomicInteger errorsCounter = new AtomicInteger();

        //and
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        List<Callable<T>> processors = new ArrayList<>();

        for (int i = 0; i < threadPoolSize; i++) {
            Callable<T> processor = new CollectionIteratorWithRemovalTryProcessor<>(collection, ThreadLocalRandom.current().nextInt(0, collection.size()));
            processors.add(processor);
        }

        //when
        List<Future<T>> results = executor.invokeAll(processors);

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.SECONDS);

        //then
        for (Future<T> result : results) {
            try {
                result.get();
            } catch (ExecutionException expected) {
                if (isCollectionImmutable) {
                    assertEquals("Processor can throw only java.lang.UnsupportedOperationException", "java.lang.UnsupportedOperationException", expected.getMessage());
                    assertNotEquals("Processor can NOT throw java.util.ConcurrentModificationException", "java.util.ConcurrentModificationException", expected.getMessage());
                } else {
                    assertEquals("Processor can throw only java.util.ConcurrentModificationException", "java.util.ConcurrentModificationException", expected.getMessage());
                    assertNotEquals("Processor can NOT throw java.lang.UnsupportedOperationException", "java.lang.UnsupportedOperationException", expected.getMessage());
                }
                errorsCounter.getAndIncrement();
            }
        }

        //and
        if (isCollectionImmutable) {
            assertEquals("Number of exception being thrown must be equal to number of processors", threadPoolSize, errorsCounter.get());
        } else if (threadPoolSize == 1) {
            assertEquals("Number of exception being thrown must be equal to 0 for one thread", 0, errorsCounter.get());
        } else {
            assertTrue("Number of exception being thrown should be close to number of threads but might be a bit less (~298, 299 depending on your hardware and collection iteration time) then exact thread number", 0 < errorsCounter.get() && errorsCounter.get() <= threadPoolSize);
        }
    }

    private Object iterationOverCollectionWithRemovalTryTestDataLoad() {
        return new Object[]{
                new Object[]{1, ImmutableList.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), true},
                new Object[]{300, ImmutableList.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), true},
                new Object[]{1, new ArrayList<>(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), false},
                new Object[]{300, new ArrayList<>(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), false},
                new Object[]{1, ImmutableSet.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), true},
                new Object[]{300, ImmutableSet.from(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), true},
                new Object[]{1, new HashSet<>(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), false},
                new Object[]{300, new HashSet<>(Arrays.asList("Red", "Yellow", "Green", "Navy", "Blue", "Pink", "Orange", "White", "Black", "Purple", "Azure")), false}
        };
    }

    protected class CollectionIteratorProcessor<E> implements Callable<E> {

        private final Collection<E> collection;
        private final List<E> elementsToBeCompared;

        CollectionIteratorProcessor(Collection<E> collection, List<E> elementsToBeCompared) {
            this.collection = collection;
            this.elementsToBeCompared = elementsToBeCompared;
        }

        @Override
        public E call() throws Exception {
            forEach(collection, (el, i, n) -> assertEquals("Actual collection element is different from the expected one", elementsToBeCompared.get(i), el));
            return null;
        }
    }

    protected class CollectionIteratorWithRemovalTryProcessor<E> implements Callable<E> {

        private final Collection<E> collection;
        private int elementToBeRemoved;

        CollectionIteratorWithRemovalTryProcessor(Collection<E> collection, int elementToBeRemoved) {
            this.collection = collection;
            this.elementToBeRemoved = elementToBeRemoved;
        }

        @Override
        public E call() throws Exception {
            Iterator<E> iterator = collection.iterator();
            int loopCounter = 0;
            while (iterator.hasNext()) {
                iterator.next();
                Thread.sleep(100);
                if (loopCounter++ == elementToBeRemoved) {
                    iterator.remove();
                }

            }
            return null;
        }
    }

}
